

<!DOCTYPE html>
<html lang="en">
<head>
   <style>
 .header{
   position:absolute;
   width: 480px;
   height: 350px;
   bottom: 40%;
   border-radius: 10px;
   padding: 40px;
   font-size: 17px;
   box-sizing: border-box;
   background:#ecf0f3;
   box-shadow: 14px 14px 20px #cbced1, -14px -14px 20px white;
   letter-spacing: 2px;
   border-left-style: outset;
   border-left-color: #1DA1F2;
   border-right-style: inset;
   border-right-color: #1DA1F2;
   
 }
 .header2{
   position:absolute;
   width: 480px;
   height: 350px;
   bottom: 5%;
   left: 60%;
   border-radius: 10px;
   padding: 40px;
   font-size: 17px;
   box-sizing: border-box;
   background:#ecf0f3;
   border-radius: 10;
   border-right-style: inset;
   border-right-color: #1DA1F2;
   border-bottom-style: inset;
   border-bottom-color: #1DA1F2;
   box-shadow: 14px 14px 20px #cbced1, -14px -14px 20px white;
   letter-spacing: 2px;
   
 }
 .header3{
   position:absolute;
   width: 480px;
   height: 350px;
   top: 40%;
   right: 60%;
   border-radius: 10px;
   padding: 40px;
   font-size: 17px;
   box-sizing: border-box;
   background:#ecf0f3;
   border-left-style: outset;
   border-left-color: #1DA1F2;
   border-bottom-style: inset;
   border-bottom-color: #1DA1F2;
   box-shadow: 14px 14px 20px #cbced1, -14px -14px 20px white;
   letter-spacing: 2px;
   
 }
 
 body {
   margin: 0;
   width: 100vw;
   height: 100vh;
   background: #ecf0f3;
   display: flex;
   align-items: center;
   text-align: center;
   justify-content: center;
   place-items: center;
   overflow: hidden;
   font-family: 'Courier New', Courier, monospace;
 }
 
 .container {
   position: relative;
   width: 500px;
   height: 100px;
   border-radius: 20px;
   padding: 40px;
   /* box-sizing: border-box; */
   /* background:#ecf0f3; */
   box-shadow: 14px 14px 20px #cbced1, -14px -14px 20px white;
   letter-spacing: 4px;
   border-right-style: inset;
   border-right-color: #1DA1F2;
   border-left-style: outset;
   border-left-color: #1DA1F2;
   
 }
 
 .inputs {
   text-align: left;
   margin-top: 3px;
 }
 
 label, input, button {
   display:inline;
   width: 100%;
   padding: 0;
   border: none;
   outline: none;
   box-sizing: border-box;
 }
 .lable2{
   margin-bottom: 40px;
   padding-right: 100%;
   font-family: 'Courier New', Courier, monospace;
 }
 
 label {
   margin-bottom: 40px;
   padding-right: 100%;
   font-family: 'Courier New', Courier, monospace;
 }
 
 label:nth-of-type(2) {
   margin-top: 12px;
   padding-right: 0%;
 }
 
 input::placeholder {
   color: gray;
 }
 
 input {
   background: #f7f9fb;
   padding: 10px;
   padding-left: 20px;
   height: 50px;
   font-size: 14px;
   border-radius: 20px;
   box-shadow: inset 6px 6px 6px #7da9d6, inset -6px -6px 6px rgb(205, 246, 249);
 }
 
 button {
   color: white;
   margin-top: 20px;
   background: #1DA1F2;
   height: 40px;
   width: 220px;
   border-radius: 20px;
   cursor: pointer;
   font-weight: 900;
   box-shadow: 6px 6px 6px #cbced1, -6px -6px 6px white;
   transition: 0.5s;
 }
 
 button:hover {
   box-shadow: none;
 }
 
 a {
   position: absolute;
   font-size: 8px;
   bottom: 4px;
   right: 4px;
   text-decoration: none;
   color: black;
   background: yellow;
   border-radius: 10px;
   padding: 2px;
 }
 
 h1 {
   position: absolute;
   top: 0;
   left: 0;
 }
 
 
   </style>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>Document</title>
</head>
<body>
    <div class="header">
        Question 21. <br><br> Write a program in C# Sharp to accept a grade and declare the equivalent description
   </div>
   <div class="header2"></div>
   <div class="header3"></div>
   <form action="Question21.html" method="POST">
   
       <DIv class="container">
                      <?php
    if($_SERVER['REQUEST_METHOD']=='POST'){

        $grade = $_POST['grade'];
        print "INPUT:"."<br>";
        print "Grade Entered: ".$grade."<br><br>";
        print "OUTPUT: "."<br>";
        switch ($grade)
        {
            case 'A+':
                print "Excellent"."<br>";
                break;
            case 'A-':
              print "Very Very Good"."<br>";
              break;
            case 'B+':
                print " Very Good"."<br>";
                break;
                case 'B-':
                  print "Good"."<br>";
                  break;
            case 'C':
                print "Average"."<br>";
                break;
            case 'D':
                print "Below Average"."<br>";
                break;
            case 'F':
                print "Fail"."<br>";
                break;
            default:
                print "Invalid Grade"."<br>";
        }
    }

                      ?>
              
                       <button type="submit">Back</button>
                
       </DIv>
   </form>
</body>
</html>